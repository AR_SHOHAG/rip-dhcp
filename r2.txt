Router>en
Router#config t
Enter configuration commands, one per line.  End with CNTL/Z.
Router(config)#int f0/0
Router(config-if)#ip add 15.15.15.1 255.252.0.0
Router(config-if)#no shut

Router>en
Router#config t
Enter configuration commands, one per line.  End with CNTL/Z.
Router(config)#int s2/0
Router(config-if)#ip add 20.20.20.22 255.255.255.252
Router(config-if)#no shut
Router(config-if)#ex

Router>en
Router#config t
Enter configuration commands, one per line.  End with CNTL/Z.
Router(config)#int f0/0
Router(config-if)#ip add 15.15.15.1 255.248.0.0
Router(config-if)#no shut

Router#config t
Enter configuration commands, one per line.  End with CNTL/Z
Router(config)#router rip
Router(config-router)#network 20.20.20.20
Router(config-router)#network 15.8.0.0
Router(config-router)#no auto-summary



